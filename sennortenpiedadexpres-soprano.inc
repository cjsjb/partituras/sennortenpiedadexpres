\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key cis \minor

		R2*2  |
		r8 gis' fis' e' 16 dis'  |
		cis' 2  |
%% 5
		r8 cis'' b' eis' 16 eis'  |
		fis' 2  |
		r8 gis' fis' e' 16 dis'  |
		cis' 2  |
		r4 cis' 8 dis'  |
%% 10
		e' 4 e'  |
		dis' 4 dis'  |
		cis' 2  |
		r4 cis' 8 dis'  |
		e' 4 e'  |
%% 15
		dis' 4 dis'  |
		cis' 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Se -- ñor, ten pie -- dad.
		Cris -- to, ten pie -- dad.
		Se -- ñor, ten pie -- dad,
		de no -- so -- tros ten pie -- dad,
		de no -- so -- tros ten pie -- dad.
	}
>>
