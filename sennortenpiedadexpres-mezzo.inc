\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key cis \minor

		R2*2  |
		r8 gis' fis' e' 16 dis'  |
		cis' 2  |
%% 5
		r8 cis' cis' cis' 16 cis'  |
		fis' 2  |
		r8 gis' fis' e' 16 dis'  |
		cis' 2  |
		r4 cis' 8 dis'  |
%% 10
		gis' 4 gis'  |
		fis' 4 fis'  |
		e' 2  |
		r4 e' 8 fis'  |
		gis' 4 gis'  |
%% 15
		fis' 4 fis'  |
		e' 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Se -- ñor, ten pie -- dad.
		Cris -- to, ten pie -- dad.
		Se -- ñor, ten pie -- dad,
		de no -- so -- tros ten pie -- dad,
		de no -- so -- tros ten pie -- dad.
	}
>>
