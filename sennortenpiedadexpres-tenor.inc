\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key cis \minor

		R2*3  |
		r8 gis fis e 16 dis  |
%% 5
		cis 2  |
		r8 cis' b eis 16 eis  |
		fis 2  |
		r8 gis fis e 16 dis  |
		cis 4 cis 8 dis  |
%% 10
		e 4 e  |
		dis 4 dis  |
		cis 2  |
		r4 e 8 fis  |
		a 4 a  |
%% 15
		bis 4 bis  |
		gis 2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Se -- ñor, ten pie -- dad.
		Cris -- to, ten pie -- dad.
		Se -- ñor, ten pie -- dad,
		de no -- so -- tros ten pie -- dad,
		de no -- so -- tros ten pie -- dad.
	}
>>
