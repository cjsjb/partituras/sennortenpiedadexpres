\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		cis2:m cis2:sus2 cis8:m r4.

		% sennor, ten piedad
		cis2:m cis2:7
		fis2:m fis2:m
		cis2:m cis2:m
		a2:maj7 gis2:7
		cis2:m cis2:m
		a2:maj7 gis2:7
		cis2:m
	}
